<?php
function avada_child_scripts() {
	if ( ! is_admin() && ! in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) ) ) {
		$theme_info = wp_get_theme();
		wp_enqueue_style( 'avada-child-stylesheet', get_template_directory_uri() . '/style.css', array(), $theme_info->get( 'Version' ) );
	}
}
add_action('wp_enqueue_scripts', 'avada_child_scripts');

/**
 * Options and functions
 */
require get_stylesheet_directory() . '/inc/misc.php';

/**
 * Where CPT are registered 
 */
require get_stylesheet_directory() . '/inc/custom-post-types.php';

/**
 * Tweaks for the 'avada_links' CPT
 */
require get_stylesheet_directory() . '/inc/links.php';

/**
 * Tweaks for the 'avada_events' CPT
 */
require get_stylesheet_directory() . '/inc/events.php';
