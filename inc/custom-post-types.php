<?php

register_post_type( 'avada_links',
	array(
		'labels' => array(
			'name' => 'Links',
			'singular_name' => 'links'
		),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => $data['links_slug']),
		'supports' => array('title', 'editor', 'thumbnail','comments'),
		'can_export' => true,
	)
);

register_taxonomy('links_category', 'avada_links',
	array(
		'hierarchical' => true,
		'label' => 'Categories',
		'query_var' => true,
		'rewrite' => true
	)
);

register_post_type( 'avada_events',
	array(
		'labels' => array(
			'name' => 'Events',
			'singular_name' => 'events'
		),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => $data['events_slug']),
		'supports' => array('title', 'editor', 'thumbnail','comments'),
		'can_export' => true,
	)
);

register_taxonomy( 'events_category', 'avada_events',
	array(
		'hierarchical' => true,
		'label' => 'Categories',
		'query_var' => true,
		'rewrite' => true
	)
);
